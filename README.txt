-------------------------------------------------------------------------------
SN Block Visualizer
-------------------------------------------------------------------------------
DESCRIPTION:
This is a module for Drupal 7 to provide better visualization of region 
and blocks in a page template. It betters the working of block and region 
mapping and uses drag and drop feature to build the page on block page.
User will be able to preview the page while assembling it, giving them the 
comfort to review the look and feel as they progress. User should be able 
to rearrange the block visually inside a region on the block page as well.

FEATURES:

* Very simple to use;
* Shows how the actual regions will be displayed with blocks data;
* Can assign/de-assign a block simply by dragging and dropping to
  and from a region repectively;

INSTALLATION:

* Put the module in your drupal modules directory and enable it in 
  admin/modules. 
  
* Go to admin/structure/block and click on "SN Block Visulaizer" TAB.
-------------------------------------------------------------------------------
